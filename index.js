// alert('hello world');

// console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

const firstName = "AJ";
	console.log('First Name: ' + firstName);

const lastName = "Morales";
	console.log('Last Name: ' + lastName);

let myAge = 22;
	console.log('Age: '+ myAge);

let hobbies = [
		'Archery, ' 	+ 
		'Sim Racing, '	+
		'Gaming'		
	]
	console.log('Hobbies: ');
	console.log(hobbies)
		
let workAddress = {
		houseNumber: 123,
		street: "J.P. Rizal",
		city: "Marikina",
		state: "Metro Manila",
	}
	console.log('Work Address: ');
	console.log(workAddress);


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let capFullName = "Steve Rogers";
	console.log("My full name is " + capFullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ");
	console.log(friends);

	let profile = {

		userName: "captain_america",
		fullName: "Steve Rogers",
		age: "40",
		isActive: false,

	}
	console.log("My Full Profile: ");
	console.log(profile);

	let winterFullName = "Bucky Barnes";
	console.log("My bestfriend is: " + winterFullName);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

